import { makeStyles } from '@material-ui/styles';
import { Typography } from '@material-ui/core';
import { PropTypes } from 'prop-types';

const useStyles = makeStyles((theme) => ({
  header: {
    fontFamily: 'Italianno',
    color: theme.palette.primary.main,
    lineHeight: 0.5,
    marginTop: 20,
    paddingTop: 20,
    borderBottom: '3px solid',
    borderBottomColor: theme.palette.gray.main,
    textShadow: '1px 2px 5px gray',
  },
}));

const Header = ({ children }) => {
  const classes = useStyles();

  return (
    <Typography className={classes.header} variant="h1">{children}</Typography>
  );
};

Header.propTypes = {
  children: PropTypes.string.isRequired,
};

export default Header;
