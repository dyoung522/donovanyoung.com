import Main from '../layouts/main';
import Header from '../components/Header';
import UnderConstruction from '../components/under_construction';

const About = () => (
  <Main currentPage="about">
    <Header>About Me</Header>
    <UnderConstruction />
  </Main>
);

export default About;
