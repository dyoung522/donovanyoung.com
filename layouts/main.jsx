import { makeStyles } from '@material-ui/styles';
import Container from '@material-ui/core/Container';
import PropTypes from 'prop-types';
import Menu from '../components/Menu';

const useStyles = makeStyles((theme) => ({
  root: {
    overflow: 'auto',
    color: theme.palette.primary.main,
  },
}));

const Main = ({ children, currentPage, showMenu }) => {
  const classes = useStyles();
  const pages = [
    { label: 'Home', path: '/', selected: currentPage === 'home' },
    { label: 'About Me', path: '/about', selected: currentPage === 'about' },
  ];

  return (
    <Container maxWidth="lg" className={classes.root}>
      {showMenu && <Menu pages={pages} />}
      {children}
    </Container>
  );
};

Main.defaultProps = {
  currentPage: '',
  showMenu: true,
};

Main.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  currentPage: PropTypes.string,
  showMenu: PropTypes.bool,
};

export default Main;
