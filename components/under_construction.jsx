import { Box, Paper, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center',
    paddingTop: 100,
    paddingBottom: 200,
    margin: theme.spacing(),
  },
  cursive: {
    fontFamily: '"Rock Salt"',
    padding: theme.spacing(4, 0, 4),
    color: 'orange',
    textShadow: '2px 3px 7px grey',
  },
  image: {
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '50%',
  },
  body: {
    marginTop: 20,
    color: theme.palette.primary.main,
  }
}));

const UnderConstruction = ({ children }) => {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>
      <img className={classes.img} src="/static/under_construction.jpg" alt="Under Construction" />
      <Typography variant="h4">This page is currently</Typography>
      <Typography className={classes.cursive} variant="h2">Under Construction</Typography>
      <Box>
        <Typography className={classes.body} variant="body1">
          {children}
        </Typography>
      </Box>
    </Paper>
  );
};

UnderConstruction.defaultProps = {
  children: 'Please check back later!',
};

UnderConstruction.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default UnderConstruction;
