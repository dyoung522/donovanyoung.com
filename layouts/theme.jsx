import { createMuiTheme } from '@material-ui/core/styles';
import { red, grey } from '@material-ui/core/colors';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#033c73',
    },
    secondary: {
      main: '#2fa4e7',
    },
    gray: {
      light: grey.A100,
      main: grey.A200,
      dark: grey.A400,
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#eee',
    },
  },
  typography: {
    fontFamily: [
      'Roboto',
      '"Rock Salt"',
    ].join(','),
  },
});

export default theme;
