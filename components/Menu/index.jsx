import Link from 'next/link';
import { makeStyles } from '@material-ui/styles';
import { Box, MenuList, MenuItem, Paper } from '@material-ui/core'; // eslint-disable-line object-curly-newline
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  box: {
    float: 'right',
  },
  paper: {
    padding: theme.spacing(),
    margin: theme.spacing(4),
    width: theme.spacing(25),
    border: 'solid 1px',
    borderColor: theme.palette.primary.main,
  },
  menuItem: {
    fontFamily: '"Rock Salt"',
    color: theme.palette.primary.main,
  },
}));

const Menu = ({ pages }) => {
  const classes = useStyles();

  return (
    <Box className={classes.box}>
      <Paper className={classes.paper}>
        <MenuList>
          {pages.map((menu) => (
            <Link key={menu.label} href={menu.path}>
              <MenuItem selected={menu.selected} className={classes.menuItem}>{menu.label}</MenuItem>
            </Link>
          ))}
        </MenuList>
      </Paper>
    </Box>
  );
};

Menu.propTypes = {
  pages: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default Menu;
