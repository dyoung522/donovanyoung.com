import Header from '../components/Header';
import Main from '../layouts/main';
import UnderConstruction from '../components/under_construction';

const Home = () => (
  <Main currentPage="home" showMenu={false}>
    <Header>Donovan C. Young</Header>
    <UnderConstruction>
      Hey, Donovan here, I&lsquo;m currently in the process of rewriting the site from scratch... so...
      <br />
      Thanks for stopping by, but please check back later!
    </UnderConstruction>
  </Main>
);

export default Home;
