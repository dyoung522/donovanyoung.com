import { makeStyles } from '@material-ui/styles';

export default makeStyles((theme) => ({
  root: {
    display: 'flex',
    color: theme.palette.primary.main,
    marginTop: 20,
  },
  cursive: {
    fontFamily: '"Rock Salt", cursive',
    color: theme.palette.primary.main,
  },
}));
